
//     Завдання

/*Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:
У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався 
конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. 
У коментарях зазначено, який текст має відображатися для якої вкладки.
Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та 
видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не 
переставала працювати.*/

// За допомогою індексів
// const activePage = document.querySelectorAll('.tabs-title');
// const activeText = document.querySelectorAll('.tabs-text');
// activePage.forEach((page, index) => {
//     page.onclick = function () {
//     for (const navLink of activePage) {
//         navLink.classList.remove("active");
//         for (const navText of activeText) {
//             navText.classList.add("active-text");
//         }
//     }
//         page.classList.add("active");
//         activeText[index].classList.remove("active-text");
       
//     }
    
// })

// За допомогою data-атрибутів

const activePage = document.querySelectorAll('.tabs-title');
const activeText = document.querySelectorAll('.tabs-text');
activePage.forEach((page) => {
    page.onclick = function () {        
    for (const navLink of activePage) {
        navLink.classList.remove("active");
        for (const navText of activeText) {
            navText.classList.add("active-text");
        }
    }
        page.classList.add("active");
        const dataTab = page.getAttribute('data-tab');
        document.getElementById(dataTab).classList.remove("active-text");
       
    }
})